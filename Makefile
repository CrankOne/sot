
CC=gcc
CXX=g++
GNUPLOT=gnuplot
CFLAGS=-g -fmax-errors=1 -Wall -fPIC -x c -Iinclude -I/usr/include/gsl -DUSE_GSL=1

LIB_SRCS=$(wildcard src/*.c )
LIB_OBJS=$(patsubst src/%.c,obj/%.o,${LIB_SRCS})

#
# Basic rules for library build

all: lib/libSot.so bin/examples bin/viewer

lib/libSot.so: ${LIB_OBJS}
	$(CC) -shared $^ -o $@

bin/examples: examples/examples.c lib/libSot.so
	${CXX} ${CFLAGS} -lgsl -lgslcblas -Llib -Llib -lSot $< -o $@ -Wl,-rpath=lib

bin/viewer: examples/viewer/camera/libCamera.a \
			examples/viewer/main.c \
			examples/viewer/s3dv_viewer.c
	${CC} ${CFLAGS} \
				-x c \
				-Iexamples/viewer/camera \
				-lglfw -lOpenGL -lGLU -lGLEW -lconfuse \
				-Llib -Llib -Lexamples/viewer/camera \
				-o $@ $(filter-out $<,$^) \
				-lm -lSot -lCamera \
				-Wl,-rpath=lib

examples/viewer/camera/libCamera.a:
	${MAKE} -C examples/viewer/camera

obj/%.o: src/%.c
	${CXX} ${CFLAGS} -c $< -o $@

#
# Documentation

figures: doc/images/parab-raytr.svg \
         doc/images/rayc-sphere.svg

doc/images/parab-raytr.svg: examples/plotting/parabolic-raytracing.gpi bin/examples
	bin/examples parrefl > fields.txt
	$(GNUPLOT) $<
	rm fields.txt

doc/images/rayc-sphere.svg: examples/plotting/raycasting-2nd.gpi bin/examples
	bin/examples 2osRayParallel > 2nd.txt
	$(GNUPLOT) $<
	rm 2nd.txt

.PHONY: all clean figures

clean:
	rm -f sot
	rm -f obj/* lib/* bin/*
	${MAKE} -C examples/viewer/camera clean
