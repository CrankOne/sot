# Simple Optics Raytracing library

A pure-C library performing strightforward raytracing/raycasting calculus for
analytic surfaces.

Developed and distributed on behalf of pure academic experiment and toy for
mind.

# Examples

Direction of parallel ray beam reflected on symmetric paraboloid surface:

![Parabolic reflections](examples/parrefl/2.svg)

For Householder's math behind, see relevant [blog entry (russian)](https://crank.qcrypt.org/otrazhenie-luchei-na-analiticheski-zadannoi-poverkhnosti.html).

Parallel beam scattered on sphere:

![Parallel on sphere](examples/2osray/sphere-simple-1.svg)



