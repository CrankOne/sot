# include <stdio.h>
# include <math.h>

# include "sot_error.h"
# include "sot_types.h"
# include "sot_refl.h"
# include "sot_refr.h"
# include "sot_surf.h"

# include <gsl_deriv.h>

/*
 * Simple math for 2-nd order surface */
static int
_dev1(int argc, char * argv[]) {
    /* 2nd order surface */
    sot_SymMatrix_t A = {{  1,  1,  0
                         ,  0,  0,  0 }};
    sot_Vector3_t l = {{ 0, 0,  1 }};
    sot_Real_t d = -2;
    /* Ray */
    sot_Vector3_t r0 = {{  .5, -.5,  0 }}
                ,  u = {{ -1,  1,  0 }}
                ;
    /* Roots */
    sot_Real_t t[2];
    char rc = sot_quad_roots( &A, &l, d, &u, &r0, 1e-7, t);
    /* Output */
    if( -1 == rc ) {
        puts("No roots.");
    } else if( 1 == rc ) {
        printf( "t_0 = t_1 = %e\n", t[0] );
    } else {
        printf( "t_0 = %e, t_1 = %e\n", t[0], t[1] );
    }
    return EXIT_SUCCESS;
}

/*
 * Intersection of the ray with 2-nd order surface */
static int
_dev2(int argc, char * argv[]) {
    /* 2nd order surface */
    sot_SymMatrix_t A = {{  1,  1,  0
                         ,  0,  0,  0 }};
    sot_Vector3_t l = {{ 0, 0,  1 }};
    sot_Real_t d = -2;
    /* Ray */
    sot_Vector3_t r0 = {{  .5, -.5,  0 }}
                ,  u = {{ -1,  1,  0 }}
                ;
    /* Roots */
    sot_Vector3_t x[2];
    sot_Real_t t[2];
    char rc = sot_ray_intersct_2nd( &A, &l, d, &u, &r0, 1e-7, t, x);
    /* Output */
    if( -1 == rc ) {
        puts("No roots.");
    } else if( 1 == rc ) {
        printf( "x_1 = x_2 = { %e, %e, %e } (t=%e)\n"
              , x[0].x, x[0].y, x[0].z, t[0] );
    } else {
        printf( "x_1 = { %e, %e, %e } (t = %e), x_2 = {%e, %e, %e} (t = %e)\n"
              , x[0].x, x[0].y, x[0].z, t[0]
              , x[1].x, x[1].y, x[1].z, t[1] );
    }
    return EXIT_SUCCESS;
}

/*
 * Parallel rays beam reflection on the sphere */
static int
_dev3(int argc, char * argv[]) {
    /* 2nd order surface: sphere of radius 1 */
    sot_SurfaceParameters_t gSph = { sot_kSecondOrder
        , .secOrd={ {{  1,  1,  1
                     ,  0,  0,  0
                     }}
                  , {{ 0, 0,  0 }}
                  , -1
                  }
        };
    /* Whether the incident light is parallel beam or point */
    char isPoint = argc == 2 && !strcmp("point", argv[1]) ? 1 : 0;
    /* Ray: from the plane or point of z=2 */
    sot_Vector3_t r0 = {{  0,  0,  2 }}
                ,  u = {{  0,  0, -1 }}
                ;
    sot_Index_t n = 7;  /* number of rays from plane per axis */
    /* Roots */
    sot_Vector3_t x[2];
    sot_Real_t t[2];
    sot_Index_t k;  /* Set to the first real intersection */
    sot_Direction_t d_i, d_o;  /* direction of incident and reflected ray */
    for( sot_Index_t i = 0; i < n; ++i ) {
        if( isPoint ) {
            u.r[0] =  i*M_PI_2/(n-1) - M_PI/4;
        } else {
            r0.r[0] = i*3./(n-1) - 1.5;
        }
        for( sot_Index_t j = 0; j < n; ++j ) {
            if( isPoint ) {
                u.r[1] =  j*M_PI_2/(n-1) - M_PI/4;
            } else {
                r0.r[1] = j*3./(n-1) - 1.5;
            }
            char rc = sot_ray_intersct_2nd( &gSph.secOrd.a, &gSph.secOrd.l, gSph.secOrd.d
                                          , &u, &r0, 1e-7, t, x);
            /* Output */
            if( -1 == rc ) {
                continue;  /* no roots => no intersection */
            } else if( 1 == rc ) {
                k = 0;
            } else {
                k = t[0] > t[1] ? 1 : 0;
                if( t[k] < 0 )
                    continue;  /* intersection lies behind the "source" */
            }
            /* Intersection found -- print the source point */
            printf( "%12.3e %12.3e %12.3e ", r0.r[0], r0.r[1], r0.r[2] );
            /* print intersection point */
            printf( "%12.3e %12.3e %12.3e "
                  , x[k].r[0] - r0.r[0]
                  , x[k].r[1] - r0.r[1]
                  , x[k].r[2] - r0.r[2] );
            /* derive direction from incident ray */
            /* ... for collinear rays it is always similar (TODO?) */
            /* find the reflected direction at the point */
            sot_direction_of( &u, &d_i );
            sot_scattered2_at( &(x[k]), &gSph, &d_i, &d_o );
            /* print the scattered direction ort vector */
            printf( "%12.3e %12.3e %12.3e\n"
                  , .3*sin(d_o.theta)*cos(d_o.phi)
                  , .3*sin(d_o.theta)*sin(d_o.phi)
                  , .3*cos(d_o.theta)
                  );
        }
    }
    return EXIT_SUCCESS;
}

/*
 * Basic reflection tests on simple centered lattice paraboloid
 */

/* Define gradient on paraboloid as a static functions */
static sot_Real_t dPar_dx( const sot_Vector3_t * v ) { return -2*(v->x); }
static sot_Real_t dPar_dy( const sot_Vector3_t * v ) { return -2*(v->y); }
static sot_Real_t dPar_dz( const sot_Vector3_t * v ) { return -1; }
static sot_SurfaceParameters_t gParStatic = { sot_kStatic, {{{dPar_dx, dPar_dy, dPar_dz }}} };
/* Define paraboloid as a second-order surface */
static sot_SurfaceParameters_t gPar2ndOS = { sot_kSecondOrder
    , .secOrd={ {{  1,  1,  0
                 ,  0,  0,  0
                 }}
              , {{ 0, 0, 1 }}
              , -2
              }
    };

# if defined(USE_GSL) && 1 == USE_GSL
/* Define paraboloid as implicit function with numerical differentiation
 * provided  by GSL with accompanying parameters*/
static sot_Real_t _implicit_paraboloid( const sot_Vector3_t * r, void * unused ){
    return 2. - r->x*r->x - r->y*r->y - r->z;
}
static sot_SurfaceParameters_t gParImplicit = { sot_kImplicit
    , .implicit = {
            _implicit_paraboloid,
            NULL,
            gsl_deriv_central,
            1e-3
        }
    };
# else
# warning GSL is off and no own numerical differentiation is currently implemented: unable to use implicit surfaces.
# endif


/* Reflection on paraboloid -- entry point */
static int
_test_simple_paraboloid(int argc, char * argv[]) {
    const sot_SurfaceParameters_t * par = NULL;
    const sot_Index_t nSamples = 9;
    const sot_Real_t s = 6./(nSamples-1)  /* step for discretization grid */
               , gvr = sqrt(s*s + s*s)  /* length of vector depicting segment */
               ;
    //const sot_Direction_t d_i = {{0, 0}};
    //sot_Direction_t d_o;
    sot_Vector3_t r  /* Point on surface */
                , g  /* Gradient vector in the point r */
                , r_i = {{0, 0, 1}} /* Incident ray unit vector */
                , r_s  /* Scattered (reflected) ray unit vector */
                , r_r  /* Refracted ray unit vector */;

    if( argc == 2 ) {
        if( !strcmp( argv[1], "static" ) ) {
            puts("# reflection & refraction on lattice paraboloid using static gradient functions");
            par = &gParStatic;
        } else if( !strcmp( argv[1], "implicit" ) ) {
            # if defined(USE_GSL) && 1 == USE_GSL
            puts("# reflection & refraction on lattice paraboloid using numerical gradient functions");
            par = &gParImplicit;
            # else
            /* TODO */
            SOT_THROW( NotImplemented, "GSL is not available -- unable to"
                    " perform test on implicit surface." )
            # endif
        } else {
            SOT_THROW( InvalidOption, "Only \"static\" and \"implicit\" options"
                    " are supported, not \"%s\".", argv[1]);
        }
    } else {
        puts("# reflection & refraction on lattice paraboloid using matrix function");
        par = &gPar2ndOS;
    }

    for( sot_Index_t i = 0; i < nSamples; ++i ) {
        for( sot_Index_t j = 0; j < nSamples; ++j ) {
            r.x = -3 + s*i;
            r.y = -3 + s*j;
            r.z = 2 - r.x*r.x - r.y*r.y;
            
            sot_nabla_at( &r, par, &g, NULL );
            sot_Real_t g2 = sot_scattered( &g, &r_i, &r_s );
            g.x /= sqrt(g2);
            g.y /= sqrt(g2);
            g.z /= sqrt(g2);
            sot_refracted( &g, &r_i, .8, &r_r );

            /* Playing around with surfaces and field */
            /* 1st triplet: point on surface (root for scattered and tip for
             * incident) */
            printf( "%12.3e %12.3e %12.3e   "
                  , r.x, r.y, r.z );
            /* 2nd triplet: incident light unit vector */
            printf( "%12.3e %12.3e %12.3e   "
                  , r_i.x, r_i.y , r_i.z );
            /* 3rd triplet: tip of scatter light direction */
            printf( "%12.3e %12.3e %12.3e   "
                  , r_s.x, r_s.y, r_s.z );
            /* 4th triplet: tip of scatter light direction */
            printf( "%12.3e %12.3e %12.3e\n"
                  , r_r.x, r_r.y, r_r.z );
        }
    }
    return EXIT_SUCCESS;
}

/*
 * Concrete example run dispatching
 ***********************************/

struct sot_ExampleEntryPoint {
    int (*run)(int, char * argv[]);
    const char * name
             , * description
             ;
} gEPs[] = {
    { _dev1, "2osRoots", "Test for second order matrix polynomial solving"
        " function and printing roots of parametric equation to terminal"
        " afterwards." },
    { _dev2, "2osX", "Test for solver finding intersection points of ray line"
        " and second order surface."  },
    { _dev3, "2osRayParallel", "Simple reflection taytracing on parallel beam"
        " and spherical surface. To see visualized picture, redirect standard"
        " output to the file `2nd.txt' and then use"
        " `examples/2osray/raycasting-2nd.gpi' Gnuplot script to build the"
        " picture." },
    { _test_simple_paraboloid, "parrefl", "Test of the parallel beam"
        " reflection on the lattice paraboloid. Use \"static\" 3rd command"
        " line argument  for static gradient functions, otherwise the"
        " matrix one will be used. To see visualization results, redirect"
        " standard output to file `fields.txt' and then use"
        " `examples/parrefl/parabolic-raytracing.gpi' Gnuplot script to build"
        " the picture." },
    { NULL, "", "" }
};

static void
print_usage( const char * appName, FILE * stream ) {
    fputs( "Usage:\n", stream );
    fprintf( stream, "  $ %s <routine> [...]\n", appName );
    fputs( "where <routine> is one of:\n", stream );
    for( struct sot_ExampleEntryPoint * c = gEPs
       ; c->run; ++c ) {
        fprintf( stream, "  * %s -- %s\n", c->name, c->description );
    }
    fputs( "Note, that some of these routines are capable to treat the command"
           " line arguments too.\n" , stream );
}

static int
_route_example( int argc, char * argv[] ) {
    if( argc < 2
      || !strcmp("-h", argv[1])
      || !strcmp("-?", argv[1])
      || !strcmp("--help", argv[1]) ) {
        puts( "Application runs choosen example from the Simple Optics Library"
               " (SOT).\nThis app consists of a set of various sample"
               " functions demonstrating basic math to perform ray-tracing"
               " procedure on various surfaces. See homepage for more"
               " explainatory references:"
               " https://bitbucket.org/CrankOne/sot/" );
        puts( "Distributed under MIT license." );
        print_usage( argv[0], stdout );
        return EXIT_SUCCESS;
    }
    for( struct sot_ExampleEntryPoint * c = gEPs
       ; c->run; ++c ) {
        if(!strcmp( c->name, argv[1] )) {
            return c->run( argc - 1, argv + 1 );
        }
    }
    fprintf( stderr, "Error: no example named \"%s\".\n", argv[1] );
    print_usage( argv[0], stderr );
    return EXIT_FAILURE;
}

int
main(int argc, char * argv[]) {
    STD_ENTRY_POINT( _route_example );
}


