# include "cam_handle.h"

# include <math.h>

/* # define CAM_DEBUG_STREAM stdout */ /* XXX */

# ifdef CAM_DEBUG_STREAM
#   include <stdio.h>
# endif

# define MULVS(v,u,s)           /* MULtiply Vector by Scalar */     {   \
    (v)[0] = (u)[0] * s;                                                \
    (v)[1] = (u)[1] * s;                                                \
    (v)[2] = (u)[2] * s;                                                \
}

# define ADDV(v,u,w)            /* ADD Vector */                    {   \
    (v)[0] = (u)[0] + w[0];                                             \
    (v)[1] = (u)[1] + w[1];                                             \
    (v)[2] = (u)[2] + w[2];                                             \
}

# define SUBV(v,u,w)            /* SUBtract Vector */               {   \
    (v)[0] = (u)[0] - w[0];                                             \
    (v)[1] = (u)[1] - w[1];                                             \
    (v)[2] = (u)[2] - w[2];                                             \
}

# define DOTVP(v, u)         /* DOT Vector Product */               (   \
    (v)[0]*(u)[0] + (v)[1]*(u)[1] + (v)[2]*(u)[2]                       \
)

# define CROSSVP(v,u,w)         /* CROSS Vector Product */          {   \
    (v)[0] = (u)[1]*(w)[2] - (u)[2]*(w)[1];                             \
    (v)[1] = (u)[2]*(w)[0] - (u)[0]*(w)[2];                             \
    (v)[2] = (u)[0]*(w)[1] - (u)[1]*(w)[0];                             \
}

# define ABSV(v)                /* ABSolute value of a Vector */    (   \
    sqrt((v)[0]*(v)[0] + (v)[1]*(v)[1] + (v)[2]*(v)[2])                 \
)

/* Quaternions */

# define CONJQ(qq, q)           /* CONJugated Quaternion */         {   \
    (qq)[0] = -(q)[0];                                                      \
    (qq)[1] = -(q)[1];                                                      \
    (qq)[2] = -(q)[2];                                                      \
    (qq)[3] =  (q)[3];                                                      \
}

# define ABSQ(q)                /* ABSolute value of a Quatern */       \
    sqrt((q)[0]*(q)[0] + (q)[2]*(q)[2] + (q)[2]*(q)[2] + (q)[3]*(q)[3]) \

# define MULQV( qq, q, t )     /* MULtiply Quaternion by Vector */  {   \
    cam_Real_t tmp[3];                                                  \
    MULVS( tmp, t, q[3] )  /* \vec{t} * w */                            \
    CROSSVP( qq, q, t )    /* \vec{v_q} \times \vec{t} */               \
    ADDV( qq, qq, tmp )    /* \vec{t}*w + \vec{v_q} \times \vec{t} */   \
    qq[3] = DOTVP(q, t );  /* \vec{v} \cdot \vec{t} */                  \
}

# define MULQQ( qq, q, t )                                        {     \
    cam_Real_t v[3], u[3];                                              \
    qq[3] = t[3]*q[3] - DOTVP( q, t );                                  \
    CROSSVP( v, q, t )                                                  \
    MULVS( qq, q, t[3] )                                                \
    MULVS( u, t, qq[3] )                                                \
    qq[0] += u[0] + v[0];                                               \
    qq[1] += u[1] + v[1];                                               \
    qq[2] += u[2] + v[2];                                               \
}

# define INVQ( qq, q )  /* Inverted quaternion */                   {   \
    cam_Real_t qnrm2 = ABSQ(q); qnrm2 *= qnrm2;                         \
    CONJQ( qq, q );                                                     \
    qq[0] /= qnrm2;                                                     \
    qq[1] /= qnrm2;                                                     \
    qq[2] /= qnrm2;                                                     \
    qq[3] /= qnrm2;                                                     \
}

static void
cam_rotation_quaternion( cam_Real_t * q
                       , cam_Real_t angle, const cam_Real_t * axis ) {
    # if 0
    Vector axis = axis_.ort();
    return Quaternion( cos(angle/2), axis*sin(angle/2) );
    # endif
    cam_Real_t sa2;
    sa2 = sin(angle*M_PI/360)/ABSV( axis );
    q[0] = axis[0]*sa2;
    q[1] = axis[1]*sa2;
    q[2] = axis[2]*sa2;
    q[3] = cos(angle*M_PI/360);
}

char
cam_yaw_inc(cam_ViewHandle_t * chPtr) {
    return cam_change_yaw( chPtr,   chPtr->yawStep );
}

char
cam_yaw_dec(cam_ViewHandle_t * chPtr) {
    return cam_change_yaw( chPtr, - chPtr->yawStep );
}

char
cam_pitch_inc(cam_ViewHandle_t * chPtr) {
    return cam_change_pitch( chPtr,   chPtr->pitchStep );
}

char
cam_pitch_dec(cam_ViewHandle_t * chPtr) {
    return cam_change_pitch( chPtr, - chPtr->pitchStep );
}

char
cam_roll_inc(cam_ViewHandle_t * chPtr) {
    return cam_change_roll( chPtr,   chPtr->pitchStep );
}

char
cam_roll_dec(cam_ViewHandle_t * chPtr) {
    return cam_change_roll( chPtr, - chPtr->pitchStep );
}

char
cam_change_yaw( cam_ViewHandle_t * chPtr, cam_Real_t angle ) {
    # if 0
    Quaternion cQ = rotation_quaternion( angle, _current._upV );
    Vector newSight = ((cQ*sight_vector())*cQ.invert()).vec();
    _current._eye = _current._LAt - newSight;
    # endif
    cam_Real_t cQ[4], aQ[4], bQ[4], sightV[3];

    SUBV( sightV, chPtr->LAt, chPtr->eye )
    cam_rotation_quaternion( cQ, angle, chPtr->upV );
    MULQV( aQ, cQ, sightV )
    INVQ( cQ, cQ )
    MULQQ( bQ, aQ, cQ )

    chPtr->eye[0] = chPtr->LAt[0] - bQ[0];
    chPtr->eye[1] = chPtr->LAt[1] - bQ[1];
    chPtr->eye[2] = chPtr->LAt[2] - bQ[2];

    return 0;
}

char
cam_change_pitch( cam_ViewHandle_t * chPtr, cam_Real_t angle ) {
    # if 0
    auto axis = (sight_vector().dot(_current._upV)).ort();
    Quaternion cQ = rotation_quaternion( angle, axis );
    Vector newSight = ((cQ*sight_vector())*cQ.invert()).vec(),
           newUp = ((cQ*_current._upV)*cQ.invert()).vec();
    _current._eye = _current._LAt - newSight;
    _current._upV = newUp;
    # endif
    cam_Real_t sightV[3], newSightQ[4]
             , axis[3], cQ[4], dQ[4], eQ[4], fQ[4]
             , nrm;

    SUBV( sightV, chPtr->LAt, chPtr->eye )
    CROSSVP( axis, sightV, chPtr->upV )
    nrm = ABSV( axis );
    axis[0] /= nrm; axis[1] /= nrm; axis[2] /= nrm;
    //printf( "cross vp: %e, %e, %e.\n", axis[0], axis[1], axis[2] );
    cam_rotation_quaternion( cQ, angle, axis );

    MULQV( dQ, cQ, sightV )
    INVQ( eQ, cQ )
    MULQQ( newSightQ, dQ, eQ )

    chPtr->eye[0] = chPtr->LAt[0] - newSightQ[0];
    chPtr->eye[1] = chPtr->LAt[1] - newSightQ[1];
    chPtr->eye[2] = chPtr->LAt[2] - newSightQ[2];

    MULQV(fQ, cQ, chPtr->upV)
    MULQQ(dQ, fQ, eQ)

    chPtr->upV[0] = dQ[0];
    chPtr->upV[1] = dQ[1];
    chPtr->upV[2] = dQ[2];

    # ifdef CAM_DEBUG_STREAM
    fprintf( CAM_DEBUG_STREAM, "Pitch change: %13.4e => |eye| = |{ %e, %e, %e }| = %e\n"
                        "                               |upV| = |{ %e, %e, %e }| = %e.\n"
          , angle
          , chPtr->eye[0], chPtr->eye[1], chPtr->eye[2], ABSV(chPtr->eye)
          , chPtr->upV[0], chPtr->upV[1], chPtr->upV[2], ABSV(chPtr->upV) );
    # endif

    return 0;
}

char
cam_change_roll(cam_ViewHandle_t * chPtr, cam_Real_t angle) {
    # if 0
    Quaternion cQ = rotation_quaternion( angle, sight_vector() );
    _current._upV = ((cQ*_current._upV)*cQ.invert()).normalize().vec();
    # endif
    cam_Real_t cQ[4], dQ[4], eQ[4], fQ[4], sightV[3], nq;
    SUBV( sightV, chPtr->LAt, chPtr->eye )
    cam_rotation_quaternion( cQ, angle, sightV );
    MULQV( dQ, cQ, chPtr->upV )
    INVQ( eQ, cQ )
    MULQQ( fQ, dQ, eQ )
    nq = ABSV( fQ );
    chPtr->upV[0] = fQ[0]/nq;
    chPtr->upV[1] = fQ[1]/nq;
    chPtr->upV[2] = fQ[2]/nq;
    # ifdef CAM_DEBUG_STREAM
    fprintf( CAM_DEBUG_STREAM, "Roll change: %e => |{ %e, %e, %e }| = %e.\n"
          , angle, chPtr->upV[0], chPtr->upV[1], chPtr->upV[2]
          , ABSV(chPtr->upV) );
    # endif
    return 0;
}

