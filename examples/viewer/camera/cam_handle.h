# ifndef H_CAM_CAMHANDLE_H
# define H_CAM_CAMHANDLE_H

/**@file cam_handle.h
 * @brief Camera-like object routines.
 *
 * This abstraction simplifies interaction with the 3D view model by offering
 * a "camera" abstraction.
 * */

/**@brief A type for camera-like handle infrastructure. */
typedef float cam_Real_t;

/**@brief Describes current view object with a set of camera-like parameters.
 *
 * One would better think of this handle as a camera-like object with
 * look-at-point. */
typedef struct cam_ViewHandle {
    cam_Real_t nearCut, farCut
             , aperture
             , LAt[3], eye[3], upV[3]
             , apertureStep, rollStep, pitchStep, yawStep, moveStep
             ;

    //cam_Real_t rMat[3][3];
} cam_ViewHandle_t;

/**@brief Increase camera handle yaw value by the step value. */
char cam_yaw_inc(cam_ViewHandle_t *);
/**@brief Decrease camera handle yaw value by the step value. */
char cam_yaw_dec(cam_ViewHandle_t *);
/**@brief Increase camera handle pitch value by the step value. */
char cam_pitch_inc(cam_ViewHandle_t *);
/**@brief Decrease camera handle pitch value by the step value. */
char cam_pitch_dec(cam_ViewHandle_t *);
/**@brief Increase camera handle roll value by the step value. */
char cam_roll_inc(cam_ViewHandle_t *);
/**@brief Decrease camera handle roll value by the step value. */
char cam_roll_dec(cam_ViewHandle_t *);
/**@brief Adds the camera "yaw" angle with value given.
 *
 * Rotates camera along local Z axis. */
char cam_change_yaw( cam_ViewHandle_t *, cam_Real_t );
/**@brief Adds the camera "pitch" angle with value given.
 *
 * Rotates camera along local Y axis. */
char cam_change_pitch( cam_ViewHandle_t *, cam_Real_t );
/**@brief Adds the camera "roll" angle with value given.
 *
 * Rotates camera along local X axis. */
char cam_change_roll( cam_ViewHandle_t *, cam_Real_t );

# if 0
template<typename Vector,
         typename ValT> void
iCamera<Vector, ValT>::_recache_rotation_matrix() {
    const Quaternion q;
    const ValT x = q.vec().x(),
               y = q.vec().y(),
               z = q.vec().z(),
               w = q.m();
    _rMat = {
        { 1 - 2*y*y - 2*z*z,        2*x*y - 2*z*w,          2*x*z + 2*y*w },
        {     2*x*y + 2*z*w,    1 - 2*x*x - 2*z*z,          2*y*z - 2*x*w },
        {     2*x*z - 2*y*w,        2*y*z + 2*x*w,      1 - 2*x*x - 2*y*y }
    };
}
# endif

# endif  /* H_CAM_CAMHANDLE_H */

