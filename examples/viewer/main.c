# include "s3dv_viewer.h"

# include <stdlib.h>

static void
cam_update_view( const cam_ViewHandle_t * h, cam_Real_t aspectRatio ) {
    # if 0
    glMatrixMode( GL_PROJECTION );
    glLoadIdentity();

    gluPerspective( h->aperture,
        (GLfloat) aspectRatio,
        h->nearCut, h->farCut );

    glMatrixMode( GL_MODELVIEW );
    glLoadIdentity();

    gluLookAt( h->LAt[0] + h->eye[0]
             , h->LAt[1] + h->eye[1]
             , h->LAt[2] + h->eye[2]
             , h->LAt[0],  h->LAt[1],  h->LAt[2]
             , h->upV[0],  h->upV[1],  h->upV[2] );
    # endif
}

# if 0
static GLfloat xxx_Cubes[] = {
    -1, -1, -1,
     0,  0,  0,
     1,  1,  1,
};
# else
float points[] = {
   0.0f,  0.5f,  0.0f,
   0.5f, -0.5f,  0.0f,
  -0.5f, -0.5f,  0.0f
};
# endif

/* entry point */

int
main(int argc, char * argv[]) {
    int rc;
    GLenum glErr;
    s3dv_Viewer_t V;

    /* TODO: support user's location of config file */
    char cfgFName[] = "examples/viewer/s3dv.cfg";
    FILE * confF = fopen( cfgFName, "r" );
    if( !confF ) {
        fprintf( stderr, "Unable to open \"%s\".\n", cfgFName );
        return EXIT_FAILURE;
    }
    rc = s3dv_make_viewer( &V, confF );
    fclose( confF );
    if(rc) {
        fprintf( stderr, "Error on configuration stage: %d.\n", rc );
        return EXIT_FAILURE;
    }

    /* Create a Vector Buffer Object that will store the vertices on
     * video memory */
    GLuint vbo = 0;
    OGL_RIF( glGenBuffers(1, &vbo) );
    # if 0
    /* Allocate space and upload the data from CPU to GPU */
    glBindBuffer( GL_ARRAY_BUFFER, vbo);
    glBufferData( GL_ARRAY_BUFFER
                , sizeof(xxx_Cubes)
                , xxx_Cubes, GL_STATIC_DRAW);
    # else
    glBindBuffer( GL_ARRAY_BUFFER, vbo);
    glBufferData( GL_ARRAY_BUFFER
                , 9 * sizeof(float)
                , points, GL_STATIC_DRAW);

    GLuint vao = 0;
    glGenVertexArrays(1, &vao);
    glBindVertexArray(vao);
    glEnableVertexAttribArray(0);
    glBindBuffer(GL_ARRAY_BUFFER, vbo);
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, NULL);    
    # endif

    while( !glfwWindowShouldClose(V.window) ) {
        //float ratio;
        //int width, height;
        //glfwGetFramebufferSize(V.window, &width, &height);
        //ratio = width / (float) height;
        //glViewport(0, 0, width, height);
        glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );

        //glUseProgram( V.shaders.programme );

        //cam_update_view( &V.ch, ratio );
        
        glBindVertexArray(vao);
        // draw points 0-3 from the currently bound VAO with current in-use shader
        glDrawArrays(GL_TRIANGLES, 0, 3);

        glfwSwapBuffers(V.window);
        /* Render continuosly. */
        glfwPollEvents();
        /* Pause the execution thread. */
        //glfwWaitEvents();
    }

    //glDeleteBuffers(1, &vbo);

    glfwDestroyWindow(V.window);
    glfwTerminate();

    return EXIT_SUCCESS;
}
