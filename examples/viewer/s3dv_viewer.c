# include "s3dv_viewer.h"

# include <confuse.h>
# include <assert.h>
# include <string.h>
# include <errno.h>
# include <stdlib.h>
# include <sys/stat.h>

/* NOTE: this code widely uses `const char * const *'. Why we habe to provide
 * explicit cast from `char **': http://c-faq.com/ansi/constmismatch.html
 */

# define FOREACH_SHADER_TYPE(m)                     \
    m(      "vertex", GL_VERTEX_SHADER )            \
    m( "tesselation", GL_TESS_CONTROL_SHADER )      \
    m(  "evaluation", GL_TESS_EVALUATION_SHADER )   \
    m(    "geometry", GL_GEOMETRY_SHADER )          \
    m(    "fragment", GL_FRAGMENT_SHADER )          \
    m(     "compute", GL_COMPUTE_SHADER )

/* OpenGL
 * ======
 * Callbacks, generic helpers, etc
 * */

static void
_error_callback( int error, const char * description ) {
    fputs(description, stderr);
}

static void
_key_callback( GLFWwindow* window, int key, int scancode, int action, int mods ) {
    if( key == GLFW_KEY_ESCAPE && action == GLFW_PRESS ) {
        glfwSetWindowShouldClose(window, GL_TRUE);
        return;
    }
    # define PRESS_OR_REPEAT ( action & (GLFW_PRESS | GLFW_REPEAT) )
    s3dv_Viewer_t * vPtr = glfwGetWindowUserPointer( window );
    if(key == GLFW_KEY_D && PRESS_OR_REPEAT) {
        cam_yaw_inc( &(vPtr->ch) );
    } else if(key == GLFW_KEY_A && PRESS_OR_REPEAT) {
        cam_yaw_dec( &(vPtr->ch) );
    } else if(key == GLFW_KEY_S && PRESS_OR_REPEAT) {
        cam_pitch_inc( &(vPtr->ch) );
    } else if(key == GLFW_KEY_W && PRESS_OR_REPEAT) {
        cam_pitch_dec( &(vPtr->ch) );
    } else if(key == GLFW_KEY_Q && PRESS_OR_REPEAT) {
        cam_roll_inc( &(vPtr->ch) );
    } else if(key == GLFW_KEY_E && PRESS_OR_REPEAT) {
        cam_roll_dec( &(vPtr->ch) );
    } /*else if(key == GLFW_KEY_R && action == GLFW_PRESS) {
    }*/
    # undef PRESS_OR_REPEAT
}

/* TODO: why setting this causes a segmentation fault? */
static void GLAPIENTRY
_gl_messaging_callback( GLenum source, GLenum type,
                        GLuint id, GLenum severity,
                        GLsizei length,
                        const GLchar* message,
                        const void * userParam ) {
    FILE * stream = ( type == GL_DEBUG_TYPE_ERROR ? stderr : stdout );
    fprintf( stream
           , "GL CALLBACK: %s type = 0x%x, severity = 0x%x, message = %s\n"
           , ( type == GL_DEBUG_TYPE_ERROR ? "** GL ERROR **" : "" )
           , type
           , severity
           , message );
}

/* Configuration
 * =============
 * Config file structure, validators, parsers, etc.
 * */

static int
_validate_core_profile( cfg_t *cfg, cfg_opt_t *opt ) {
    char * v = cfg_opt_getnstr(opt, cfg_opt_size(opt) - 1);
    int isOk = 0;
    if( !strcmp("core",   v) ) isOk |= 1;
    if( !strcmp("compat", v) ) isOk |= 1;
    if( !strcmp("any",    v) ) isOk |= 1;
    return !isOk;
}

static int
_blending_int2opt( int opt ) {
    if(2 == opt) return GL_FASTEST;
    if(3 == opt) return GL_NICEST;
    return GL_DONT_CARE;
}

static cfg_t *
_mk_config() {
    static
    /* Camera stuff */
    cfg_opt_t camSteps[] = {
        CFG_FLOAT("aperture", 0, CFGF_NONE),
        CFG_FLOAT("roll",     0, CFGF_NONE),
        CFG_FLOAT("pitch",    0, CFGF_NONE),
        CFG_FLOAT("yaw",      0, CFGF_NONE),
        CFG_FLOAT("move",     0, CFGF_NONE),
        CFG_END()
    }, camOpts[] = {
		CFG_FLOAT_LIST("cuts",  0, CFGF_NONE),
        CFG_FLOAT("aperture",   0, CFGF_NONE),
        CFG_FLOAT_LIST("LAt",   0, CFGF_NONE),
        CFG_FLOAT_LIST("eye",   0, CFGF_NONE),
        CFG_FLOAT_LIST("upV",   0, CFGF_NONE),
        CFG_SEC( "steps", camSteps, CFGF_NONE ),
        CFG_END()
	},
    /* OpenGL */
    windowPars[] = {
        CFG_INT( "width",   0, CFGF_NONE ),
        CFG_INT( "height",  0, CFGF_NONE ),
        CFG_STR( "title",   0, CFGF_NONE ),
        CFG_END()
    }, blendingPars[] = {
        CFG_INT( "point",   0, CFGF_NONE ),
        CFG_INT( "line",    0, CFGF_NONE ),
        CFG_INT( "polygon", 0, CFGF_NONE ),
        CFG_END()
    }, smoothOpts[] = {
        CFG_INT(  "multisampling", 0, CFGF_NONE ),
        CFG_BOOL( "enableBlending", 0, CFGF_NONE ),
        CFG_SEC(  "blending", blendingPars, CFGF_NONE),
        CFG_END()
    }, glProgramOpts[] = {
        # define m_add_shdrlst(nm, enm) CFG_STR_LIST( nm, 0, CFGF_NONE ),
        FOREACH_SHADER_TYPE( m_add_shdrlst )
        # undef m_add_shdrlst
        CFG_END()
    }, shadersOpts[] = {
        CFG_STR_LIST( "baseDirs",   0, CFGF_NONE ),
        CFG_SEC( "programme", glProgramOpts, CFGF_TITLE | CFGF_MULTI ),
        CFG_END()
    }, oglOpts[] = {
        CFG_BOOL( "debugDump",  cfg_false,  CFGF_NONE ),
        CFG_BOOL( "fwdCompat",  cfg_true,   CFGF_NONE ),
        CFG_STR(  "profile",    0,          CFGF_NOCASE ),
        CFG_FLOAT_LIST( "contextVersion", 0, CFGF_NONE),
        CFG_SEC(  "window",     windowPars, CFGF_NONE ),
        CFG_SEC(  "smoothing",  smoothOpts, CFGF_NONE ),
        CFG_SEC(  "shaders",    shadersOpts, CFGF_NONE ),
        CFG_END(),
    },  /* Full cgf */ opts[] = {
        CFG_SEC( "camera", camOpts, CFGF_NONE ),
        CFG_SEC( "OpenGL", oglOpts, CFGF_NONE ),
        CFG_END()
    };
    cfg_t * cfg = cfg_init( opts, CFGF_NONE );
    cfg_set_validate_func(cfg, "OpenGL|profile", _validate_core_profile);
    return cfg;
}

static void
_configure_camera( cam_ViewHandle_t * ch
                , cfg_t * cfg ) {
    assert( ch );
    assert( cfg );
    int i;
    cfg_t * stp;
    ch->nearCut = cfg_getnfloat(cfg, "cuts", 0);
    ch->farCut = cfg_getnfloat(cfg, "cuts", 1);
    ch->aperture = cfg_getfloat(cfg, "aperture");
    for( i = 0; i < 3; ++i ) { ch->LAt[i] = cfg_getnfloat( cfg, "LAt", i ); }
    for( i = 0; i < 3; ++i ) { ch->eye[i] = cfg_getnfloat( cfg, "eye", i ); }
    for( i = 0; i < 3; ++i ) { ch->upV[i] = cfg_getnfloat( cfg, "upV", i ); }
    stp = cfg_getsec(cfg, "steps");
    ch->apertureStep = cfg_getfloat(stp, "aperture");
    ch->rollStep     = cfg_getfloat(stp, "roll");
    ch->pitchStep    = cfg_getfloat(stp, "pitch");
    ch->yawStep      = cfg_getfloat(stp, "yaw");
    ch->moveStep     = cfg_getfloat(stp, "move");
}

static int
_configure_opengl( s3dv_Viewer_t * v
                 , cfg_t * cfg ) {
    assert( v );
    assert( cfg );
    int rc, ctxVer = 0, mSampl, nBlending;
    GLenum glErr;
    cfg_t * smoothCfg = cfg_getsec(cfg, "smoothing")
        , * windowCfg = cfg_getsec(cfg, "window")
        , * blendingCfg = NULL
        ;

    glfwSetErrorCallback(_error_callback);
    if(!(rc = glfwInit())) {
        fprintf( stderr, "Failed to initialize GLFW: %d.\n", rc );
        return rc;
    }

    if(cfg_size(cfg, "contextVersion")) {
        ctxVer = cfg_getnfloat(cfg, "contextVersion", 0);
        OGL_RIF( glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, ctxVer) );
        OGL_RIF( glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, cfg_getnfloat(cfg, "contextVersion", 1)) );
    }
    if( cfg_getbool( cfg, "fwdCompat" ) ) {
        OGL_RIF( glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE) );
    }
    {
        /* Note: using this without `contextVersion' will cause an error since
         * context profiles are only defined for OpenGL version 3.2 and above. */
        char * profileCfg = cfg_getstr( cfg, "profile" );
        int profile;
        if( !strcmp("core", profileCfg) ) {
            profile = GLFW_OPENGL_CORE_PROFILE;
        } else if( !strcmp("compat", profileCfg) ) {
            profile = GLFW_OPENGL_COMPAT_PROFILE;
        } else if( !strcmp("any", profileCfg) ) {
            profile = GLFW_OPENGL_ANY_PROFILE;
        }
        OGL_RIF( glfwWindowHint(GLFW_OPENGL_PROFILE, profile) );
    }

    mSampl = cfg_getint(smoothCfg, "multisampling");
    if( mSampl > 0 ) {
        /* Enable multisampling for anti-aliasing, set # of samples to 4 */
        OGL_RIF( glfwWindowHint( GLFW_SAMPLES, mSampl ) );
        OGL_RIF( glEnable( GL_MULTISAMPLE ) );
    }

    /* // Nice feature of OGL 3 -- monitor mode.
     * // See also: http://www.glfw.org/docs/latest/group__monitor.html#ga820b0ce9a5237d645ea7cbb4bd383458
    GLFWmonitor* mon = glfwGetPrimaryMonitor();
    const GLFWvidmode* vmode = glfwGetVideoMode(mon);
    GLFWwindow* window = glfwCreateWindow(
      vmode->width, vmode->height, "Extended GL Init", mon, NULL
    );
     */
    if( !(v->window = glfwCreateWindow( cfg_getint(windowCfg, "width")
                                      , cfg_getint(windowCfg, "height")
                                      , cfg_getstr(windowCfg, "title"), NULL, NULL)) ) {
        glfwTerminate();
        fputs( "Failed to instantiate GLFW window.\n", stderr );
        return -1;
    }

    OGL_RIF( glfwSetWindowUserPointer( v->window, v ) );
    OGL_RIF( glfwMakeContextCurrent(v->window) );

    glewExperimental = GL_TRUE;  /* TODO: control by config option */
    if( GLEW_OK != (rc = glewInit() )) {
        fprintf( stderr, "Failed to initialize GLEW: %d.\n", rc );
        return -2;
    }

    /* TODO: do we need to keeps stdout clear? */
    printf("Renderer ................... : %s\n", glGetString(GL_RENDERER));
    printf("OpenGL version supported ... : %s\n", glGetString(GL_VERSION));

    if( cfg_getbool( cfg, "debugDump" ) ) {
        OGL_RIF( glEnable( GL_DEBUG_OUTPUT ) );
        OGL_RIF( glDebugMessageCallback( _gl_messaging_callback, NULL ) );
    }

    OGL_RIF( glfwSetKeyCallback(v->window, _key_callback) );

    if( cfg_getbool( smoothCfg, "enableBlending") ) {
        blendingCfg = cfg_getsec( smoothCfg, "blending" );
        OGL_RIF( glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA) );
        OGL_RIF( glEnable(GL_BLEND) );
        
        nBlending = cfg_getint( blendingCfg, "point" );
        if( nBlending > 0 ) {
            OGL_RIF( glEnable(GL_POINT_SMOOTH) );
            OGL_RIF( glHint(GL_POINT_SMOOTH_HINT, _blending_int2opt(nBlending)) );
        }
        
        nBlending = cfg_getint( blendingCfg, "line" );
        if( nBlending > 0 ) {
            OGL_RIF( glEnable(GL_LINE_SMOOTH) );
            OGL_RIF( glHint(GL_LINE_SMOOTH_HINT, _blending_int2opt(nBlending)) );
        }

        nBlending = cfg_getint( blendingCfg, "polygon" );
        if( nBlending > 0 ) {
            OGL_RIF( glEnable(GL_POLYGON_SMOOTH) );
            OGL_RIF( glHint(GL_POLYGON_SMOOTH_HINT, _blending_int2opt(nBlending)) );
        }
    }

    /* TODO: make this controllable via config? */
    /* Tell GL to only draw onto a pixel if the shape is closer to the viewer */
    glEnable(GL_DEPTH_TEST); /* enable depth-testing */
    glDepthFunc(GL_LESS); /* depth-testing interprets a smaller value as "closer" */

    return 0;
}

/* Utility functions
 * =================
 * Viewer interface stuff.
 * */

char *
s3dv_load_text_file( const char * filePath
                   , char ** buffer, size_t * bufSize
                   , FILE * errorStream ) {
    size_t length, nReadBytes;
    FILE * f;

    assert( filePath );
    assert( buffer );
    assert( bufSize );

    f = fopen( filePath, "rb" );
    if( !f ) {
        fprintf( errorStream, "Unable to open file by path \"%s\": %s\n"
               , filePath, strerror(errno) );
        return NULL;
    }
    fseek( f, 0, SEEK_END );
    length = ftell(f);
    fseek( f, 0, SEEK_SET );
    if( NULL == *buffer || *bufSize <= length ) {
        /* (re-)allocate buffer */
        if( NULL != *buffer ) {
            free( *buffer );
        }
        *buffer = malloc( *bufSize = (length+1) );
        if( ! *buffer ) {
            fprintf( errorStream, "While reading file \"%s\": unable to"
                    " (re-)allocate buffer of size %zu due to error \"%s\"\n"
                   , filePath, length, strerror(errno) );
            return NULL;
        }
    }
    if( length != (nReadBytes = fread( *buffer, 1, length, f )) ) {
        fprintf( errorStream, "Warning: while reading file \"%s\" expected"
                " to read data of size %zu while read %zu: %s\n"
                , filePath, length, nReadBytes
                , strerror(ferror(f)) );
    }
    fclose( f );
    (*buffer)[length] = '\0';
    return *buffer;
}

static void
_print_shader_info_log( GLuint shader_index
                      , FILE * targetStream ) {
    int max_length = 2048;
    int actual_length = 0;
    char shader_log[2048];
    glGetShaderInfoLog( shader_index
                      , max_length, &actual_length
                      , shader_log);
    printf( "Shader info log for GL index %u:\n%s\n"
          , shader_index, shader_log );
}

int
s3dv_mk_shader_from_buffer( const char * const * buffer
                          , int shaderType
                          , GLuint * sName
                          , FILE * infoStream
                          , FILE * errorStream ) {
    int p = -1;
    *sName = glCreateShader( shaderType );
    glShaderSource( *sName, 1, buffer, NULL);
    glCompileShader( *sName );
    glGetShaderiv( *sName, GL_COMPILE_STATUS, &p );
    if( GL_TRUE != p ) {
        fprintf( errorStream, "GL shader index %i did not"
                " compile.\n", *sName );
        _print_shader_info_log( *sName, errorStream );
        fprintf( errorStream, "Shader source dump:\n%s", *buffer );
        return -1;
    } else {
        if( infoStream ) {
            _print_shader_info_log( *sName, infoStream );
        }
    }
    return 0;
}

static void
_print_programme_info_log(GLuint programme) {
  int max_length = 2048;
  int actual_length = 0;
  char program_log[2048];
  glGetProgramInfoLog(programme, max_length, &actual_length, program_log);
  printf("program info log for GL index %u:\n%s", programme, program_log);
}

static int
_find_shader_file( cfg_t * cfg
                 , const char * shaderFileName
                 , char * nameBuf, size_t nameBufSize
                 , const char * const * baseDirs ) {
    struct stat statBf;
    for( const char * const * bd = baseDirs
       ; NULL != *bd
       ; ++bd ) {
        snprintf( nameBuf, nameBufSize, "%s/%s"
                , *bd, shaderFileName );
        if( 0 == stat(nameBuf, &statBf)) {
            return 0;
        }
    }
    return -1;
}

static int
_find_and_compile_shaders( cfg_t * cfg
                         , GLuint ** nms
                         , const char * shadersTypeName, int shadersType
                         , char ** strBuffer, size_t * strBufferSize
                         , const char * const * baseDirs ) {
    char nameBuf[512], n;
    char * shadersFile, * src;
    for( n = 0; n < cfg_size(cfg, shadersTypeName ); ++n ) {
        if( _find_shader_file( cfg, shadersFile = cfg_getnstr(cfg, shadersTypeName, n)
                             , nameBuf, sizeof(nameBuf)
                             , baseDirs ) ) {
            fprintf( stderr, "Unable to locate file \"%s\".\n", shadersFile );
            return -1;
        }
        src = s3dv_load_text_file( nameBuf, strBuffer, strBufferSize, stderr );
        if( !src ) {
            fprintf( stderr, "Failed to load source \"%s\".\n", nameBuf );
            return -2;
        }
        if( s3dv_mk_shader_from_buffer( (const char * const *) strBuffer, shadersType, (*nms)++
                                  , NULL, stderr ) ) {
            fprintf( stderr, "Unable to make shader from \"%s\".\n", nameBuf );
            return -3;
        }
    }
    return 0;
}

static int
_compile_shaders( struct s4dv_ShadersRegistry * reg
                , cfg_t * cfg ) {
    int rc = 0, n, m;
    cfg_t * prgSec;
    GLuint * shdrs;
    char * strBuffer = NULL, ** baseDirs;
    size_t strBufferSize = 0, nBaseDirs;

    /* Count shaders to load and allocate index.
     * Print warning and return if nothing is defined. */
    reg->nShaders = reg->nProgrammata = 0;
    for( m = 0; m < cfg_size(cfg, "programme"); ++m ) {
        prgSec = cfg_getnsec( cfg, "programme", m );
        # define m_count_shader( nm, enm ) reg->nShaders += cfg_size(  prgSec, nm );
        FOREACH_SHADER_TYPE( m_count_shader )
        # undef m_count_shader
    }
    reg->nProgrammata = cfg_size(cfg, "programme");

    if( 0 == reg->nShaders ) {
        reg->nShaders = 0;
        reg->shaders = NULL;
        fputs( "No shaders were defined for viewer in config!\n", stderr );
        return 0;
    }
    reg->shaders = malloc( sizeof(*reg->shaders) * reg->nShaders );
    reg->programmata = malloc( sizeof(reg->programmata) * reg->nProgrammata );

    /* Initialize list of base directories */
    nBaseDirs = cfg_size( cfg, "baseDirs" );
    baseDirs = malloc( sizeof(char *)*(nBaseDirs + 1) );
    for( n = 0; n < nBaseDirs; ++n ) {
        baseDirs[n] = strdup( cfg_getnstr( cfg, "baseDirs", n ) );
    }
    baseDirs[n] = NULL;

    /* Load shaders */
    shdrs = reg->shaders;

    /* Shader programme name */
    for( m = 0; m < cfg_size(cfg, "programme"); ++m ) {
        prgSec = cfg_getnsec( cfg, "programme", m );

        # define m_obtain_shaders( nm, enm ) \
        rc |= _find_and_compile_shaders( prgSec, &shdrs, nm, enm \
                                       , &strBuffer, &strBufferSize \
                                       , (const char * const *) baseDirs );
        FOREACH_SHADER_TYPE( m_obtain_shaders )
        # undef m_obtain_shaders
        if(rc) {
            fprintf( stderr, "Error while compiling shader(s) for program #%d.\n"
                   , m );
            return -1;
        }

        reg->programmata[m] = glCreateProgram();
        for( n = 0; n < reg->nShaders; ++n ) {
            glAttachShader( reg->programmata[m], reg->shaders[n] );
        }
        glLinkProgram( reg->programmata[m] );
        /* check if link was successful */
        glGetProgramiv(reg->programmata[m], GL_LINK_STATUS, &rc);
        if( GL_TRUE != rc ) {
            fprintf( stderr, "ERROR: could not link shader programme GL index %u\n"
                   , reg->programmata[m] );
            _print_programme_info_log( reg->programmata[m] );
            return -1;
        }
    }
    return 0;
}

int
s3dv_make_viewer( s3dv_Viewer_t * v
                , FILE * cfgF ) {
    int rc;
    cfg_t * cfg = _mk_config()
        , * openglCfg;
    rc = cfg_parse_fp( cfg, cfgF );
    if( rc ) {
        fprintf( stderr, "Failed to configure. libconfuse's cfg_parse()"
                " returned %d.", rc );
    }
    _configure_camera( &(v->ch), cfg_getsec(cfg, "camera") );
    rc = _configure_opengl( v, openglCfg = cfg_getsec(cfg, "OpenGL") );
    if( rc ) return rc;
    rc = _compile_shaders( &(v->shadersRegistry), cfg_getsec(openglCfg, "shaders") );
    /* TODO: ... configure GUI? */
    return rc;
}

