# ifndef H_S3DV_VIEWER_H
# define H_S3DV_VIEWER_H

# include <GL/glew.h>
# include <GLFW/glfw3.h>

# include "cam_handle.h"

# include <stdio.h>

/**@file s3dv_viewer.h
 *
 * Simple 3D viewer for OpenGL based on GLFW.
 *
 * This merely an application designed for display of various 3D
 * structures. Not reach for features by default, but rather offers an
 * extensible and configurable boilerplate for applications.
 * */

# define OGL_RIF( doable ) \
    do { doable; \
    while((glErr = glGetError()) != GL_NO_ERROR) { \
        fprintf(stderr, "OpenGL error: %#x; location: %s:%d\n", (int) glErr, __FILE__, __LINE__ ); \
    } } while(0);

/**@brief A viewer representation.
 *
 * Bounds the consisting set of window, context and "camera handle" in
 * one object. */
typedef struct s3dv_Viewer {
    /**@brief Window pointer instance. */
    GLFWwindow * window;
    /**@brief "Camera" handle instance. */
    cam_ViewHandle_t ch;
    /**@brief Shaders registry. */
    struct s4dv_ShadersRegistry {
        GLuint * shaders
             , * programmata
             ;
        size_t nShaders
             , nProgrammata
             ;
    } shadersRegistry;
} s3dv_Viewer_t;


/**@brief Initializes the new viewer object.
 *
 * With given config, does the following steps:
 *  1. Sets the error callback to the custom one, unless config option does not
 *  prohibit it;
 *  2. Calls glfwInit() checking its return code. If not 0, writes notification
 *  to stderr and returns it.
 *  3. Sets the window hints according to config.
 *  4. Creates window, checks the result. On failure, terminates GLFW,
 *  returns -1.
 *  5. Associates window the viewer pointer with window (uses
 *  glfwSetWindowUserPointer()).
 * According to config file, the particular behaviour may include other steps.
 * */
int
s3dv_make_viewer( s3dv_Viewer_t *
                , FILE * cfgF );


/**@brief Loads file entirely in the buffer.
 *
 * Typically used to load the shader raw text files. Designed to be used with
 * reentrant buffer provided by \c buffer pointer. If buffer is not sufficient
 * or points to NULL, it will be re-allocated to fit the size of the file.
 *
 * Returns pointer to (new or old) buffer, also re-writing \c buffer and
 * \c bufSize variables if need. On file open error, returns NULL and prints
 * the \c strerror to \c stderr.
 * */
char *
s3dv_load_text_file( const char * filePath
                   , char ** buffer, size_t * bufSize
                   , FILE * errorStream );


/**@brief Compiles shader source from the given buffer.
 *
 * Upon successful compilation, returns 0. If infoStream is non-NULL, dumps the
 * shader info log to it.
 *
 * If shader did not compile, returns non-zero exit code and prints the 
 * shader info log to errorStream instead.
 */
int
s3dv_mk_shader_from_buffer( const char * const * buffer
                          , int shaderType
                          , GLuint * sName
                          , FILE * infoStream
                          , FILE * errorStream );


# endif  /* H_S3DV_VIEWER_H */
