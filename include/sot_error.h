# ifndef H_SOT_ERROR_H
# define H_SOT_ERROR_H

/**@brief Defines error handling mechanism for SOT library.
 * @file sot_error.h
 *
 * The SOT library relies on
 * <a href="https://gcc.gnu.org/wiki/WindowsGCCImprovements">set jump / long jump error handling mechanism</a>.
 * Though it provides a conveniet error handling, the practical utilization
 * leads to performance penalties due to some hidden stack introspection info
 * present even at normal evaluation. Taking into account this, we made this
 * behaviour optional, controlled by SOT_NOEXCEPT macro.
 *
 * Most of the time the SOT code does not provide the actual "handling" of
 * the error, i.e. SJLJ mechanism is used to report of fatal errors setting
 * the SOT in undetermined state. However such a demand may occur in future
 * development, so the SJLJ may potentially provide a solution. So far the
 * overall error handling in SOT is driven by NDEBUG and SOT_NOEXCEPT
 * macros.
 *
 * The SOT_NOEXCEPT enables SJLJ C++ exception-like error handling and is
 * probably not desirable during performant usage.
 *
 * The standard NDEBUG macro controls C assertions.
 * */

# include <stdlib.h>
# include <string.h>

/**@brief Macro driving the error handling.
 * @def SOT_NOEXCEPT
 *
 * This macro disables SJLJ (set jump/long jump) error handling and must be
 * enabled for performant applications.
 */
# ifndef SOT_NOEXCEPT

# include <setjmp.h>

/**@brief Short descriptive code for errors in SOT.
 *
 * Error code might be used in user code to treat various errors differently.
 * */
typedef enum sot_ErrorCode {
    /** Generic or self-descriptive error. */
    sot_kGenericError = 1,
    /** Run-time error thrown when execution comes to some not implemented
     * functionality. */
    sot_kNotImplementedError = 2,
    /** Input argument(s) or option(s) are not valid for routine called. */
    sot_kInvalidOptionError = 3,
    /** Static pool exceeded error. */
    sot_kPoolOverflowError = 4,
    /* ... */
} sot_ErrorCode_t;

/**@brief Global SJLJ buffer variable for the main thread. */
extern jmp_buf sotJmpBuf;

/**@brief Error information struct.
 *
 * Keeps track of the particular place in code where the error occured, bears
 * the human-readable message.
 * */
extern struct sot_ErrDescript {
    /** The numeric code of the error for programmatic decisions. */
    sot_ErrorCode_t code;
    char message[1024]  /**@brief The human-readable message */
       , function[256]  /**@brief name of function where the error occured */
       , file[128]  /**@brief name of source code file where the error occured */
       ;
    unsigned int line;  /**@brief line number within the source file where the error occured */
} sot_errDescript;

/**@var sot_ErrDescript sot_errDescript
 * @brief Global error description instance for the main rhread. */

/**@brief Mimics the C++ try-statement (SJLJ), see error sot_error.h details. */
# define SOT_TRY    setjmp(sotJmpBuf)

/**@brief Mimics the C++ throw-statement (SJLJ), see error sot_error.h details.
 *
 * Internally, forms the sot_ErrDescript struct instance, bearing some
 * introspective info about error occured. */
# define SOT_THROW( errCode, errMsgFmt, ... ) do {                              \
        snprintf( sot_errDescript.message, sizeof(sot_errDescript.message), errMsgFmt, ##__VA_ARGS__ );  \
        strncpy( sot_errDescript.function, __FUNCTION__, sizeof(sot_errDescript.function) ); \
        strncpy( sot_errDescript.file, __FILE__, sizeof(sot_errDescript.file) );  \
        sot_errDescript.line = __LINE__;                                        \
        longjmp(sotJmpBuf, sot_errDescript.code = sot_k ## errCode ## Error );  \
    } while(0);

/**@brief Standard implementation of the entry point with SJLJ.
 *
 * For given function, expans the encompassing code that implements a primitive
 * try-catch analogue by the means of SJLJ (see sot_error.h).
 * */
# define STD_ENTRY_POINT( func )                                                \
    if(SOT_TRY) {                                                               \
        fprintf( stderr, "Fatal error at %s:%d, %s: \"%s\"\n"                   \
               , sot_errDescript.file, sot_errDescript.line                     \
               , sot_errDescript.function                                       \
               , sot_errDescript.message );                                     \
        return EXIT_FAILURE;                                                    \
    } else {                                                                    \
        return func( argc, argv );                                              \
    }

# else  /* SOT_NOEXCEPT */
# define SOT_TRY
# define SOT_THROW( errCode, errMsgFmt, ... ) do {                          \
    fprintf( stderr, "Fatal error at %s:%d, %s:\n", __FILE__, __LINE__, __FUNCTION__ );  \
    fprintf( stderr, errMsgFmt "\n", ##__VA_ARGS__ );                       \
    abort();                                                                \
} while(0);
# define STD_ENTRY_POINT( func ) return func(argc, argv)
# endif  /* SOT_NOEXCEPT */

# endif  /* H_SOT_ERROR_H */
