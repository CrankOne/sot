# ifndef H_SOT_FRESNEL_EXPRESSIONS_H
# define H_SOT_FRESNEL_EXPRESSIONS_H

# include "sot_types.h"

/**@file sot_fresnel.h
 * @brief Fresnel expressions for light transmission through the interface.
 *
 * Here we consider polarisation of the incident (i subscript), reflected (r
 * subscript) and refracted (t subscript is for "transmitted") waves in the
 * plane defined by incident ray and the surface normal vector. Within the
 * plane of incidence we derive "P-polarization". Perpendicular to P is the
 * "S-polarization". These formulae are useful in particular for calculating
 * (relative) intensity of incident and reflecting lite.
 *
 * \image html fresnel_contrib.png "S- and P- the decomposition. Credits: Alexander I. Lvovsky's `Fresnel Equations'"
 *
 * The Brewster's angle (total internal reflection) in combination with Snell's
 * law is naturally imposed within this formulae.
 *
 * The resulting formulaes are the precise solution for Maxwell's equations
 * with appropriate boundary conditions.
 *
 * For detailed reference in the current notations, see e.g.
 * <a href="http://iqst.ca/quantech/pubs/2013/fresnel-eoe.pdf">Alexander I. Lvovsky's "Fresnel Equations"</a>
 *
 * @todo Goos-Hänchen shifting effect
 * @todo Phase shift
 * */

/**@brief Generic Fresnel formulae of S- and P-polarized waves
 *
 * Calculates the set of ratio coefficients for reflected and transmitted
 * (refracted) components for polarization components at the interface of two
 * media. Note, that for non-magnetic media the less-generic function
 * sot_fresnel_ratios() might be computationally-preferable as it does less
 * arithmetics.
 *
 * Index of refraction in medium is given by \f$n = c \sqrt{\epsilon \mu}\f$.
 *
 * @arg \c n1, \c n2 Refractive index of the media.
 * @arg \c mu1, \c mu2 Corresponds tu magnetic permeability of media
 * @arg \c cosThetaI, \c cosThetaT are the cosines of reflection and
 * transmission angles
 *
 * @return \c r_P: \f$r_P = E_r/E_i\f$, reflection coefficient for P-wave
 * @return \c t_P: \f$t_P = E_t/E_i\f$, transmission coefficient for P-wave
 * @return \c r_S: \f$r_S = E_r/E_i\f$, reflection coefficient for S-wave
 * @return \c t_S: \f$t_S = E_t/E_i\f$, transmission coefficient for S-wave
 *
 * @warning For non-magnetic materials, where \f$\mu_1 = \mu_2 = \mu_0\f$
 * consider sot_fresnel_ratios() function instead.
 */
void
sot_fresnel_ratios_generic( sot_Real_t n1, sot_Real_t mu1
                          , sot_Real_t n2, sot_Real_t mu2
                          , sot_Real_t cosThetaT, sot_Real_t cosThetaI
                          , sot_Real_t * r_P, sot_Real_t * t_P
                          , sot_Real_t * r_S, sot_Real_t * t_S );

/**@brief important case of Fresnel formulae for non-magnetic materials.
 *
 * Same as sot_fresnel_ratios_generic() but for important case of non-magnetic
 * materials: \f$\mu_1 = \mu_2 = \mu_0\f$.
 * */
void
sot_fresnel_ratios( sot_Real_t thetaI, sot_Real_t thetaT
                  , sot_Real_t * r_P, sot_Real_t * t_P
                  , sot_Real_t * r_S, sot_Real_t * t_S );

# endif  /* H_SOT_FRESNEL_EXPRESSIONS_H */

