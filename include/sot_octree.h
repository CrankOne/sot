# ifndef H_SOT_OCTREE_H
# define H_SOT_OCTREE_H

# include "sot_types.h"

/**@file sot_octree.h
 * @brief Routines for spatial voxel tree.
 *
 * The goal of this implementation is to provide octree structures with small
 * memory footrpint.
 *
 * The numbering of the octants within the voxel follows the rule that last
 * three bits in a bitmask denotes the shift 1/2 of the voxel size from origin.
 *
 * \image html octree_enumeration.svg
 * */


/** 8-bit type storing the subdivisions */
typedef unsigned char sot_OctreeBits8;

/** Type indexing the octree nodes */
typedef size_t sot_OctreeNodeNum;

/** Returns bitmask marking the octants where the body part exist.
 *
 * This function used to identify the presence of body volume within the
 * cubic space segment given by bgn and end vectors. Returning bitmask
 * describes sub-quadrants to be allocated.
 *
 * There is a special case when the entire quadrant is immersed into the body
 * hull (no gaps left in the cube). In this case, the function has to return 0
 * bitmask.
 * */
typedef struct sot_Body {
    void * p;
    sot_OctreeBits8 (*intersects_f)( const void *
                                   , const sot_Vector3_t *, const sot_Vector3_t *);
} sot_Body_t;

/**@brief Octree node struct
 * 
 * Each octant is represented with its 8 sub-octants. The `fillBits' denotes
 * which of them are (partially) filled, while `octants' member refers to the
 * array of them. So, for `fillBits' == 0x1, 0x2, 0x8, the `octants' must
 * contain single element.
 *
 * There might be an exceptional case of `fillBits' = 0x0, meaning that current
 * node is fully filled by medium.
 * */
typedef struct sot_Octree {
    sot_OctreeBits8 fillBits;
    sot_OctreeNodeNum * nodes;
} sot_Octree_t;

/**@brief Octree pool.
 *
 * It is recommended to keep all the tree nodes in continous memory chunk(s)
 * (pool) to benefit from the CPU cache (by avoiding fragmentation).
 * */
typedef struct sot_OctreePool {
    /* Nodes pool */
    sot_Octree_t * elements;
    sot_OctreeNodeNum poolSize
                    , occupied;
    /* Indices pool */
    sot_OctreeNodeNum * indices;
    size_t indicesSize
         , indicesOccupied;
} sot_OctreePool_t;

/** Allocate the node from pool */
sot_Octree_t *
sot_octree_pool_alloc( sot_OctreePool_t * );

size_t
sot_make_octree( sot_OctreePool_t * pool
               , sot_Body_t * body
               , const sot_Vector3_t * O, sot_Real_t side, sot_Real_t smallestSide );

# endif  /* H_SOT_OCTREE_H */

