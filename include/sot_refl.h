# ifndef H_SOT_REFL_H
# define H_SOT_REFL_H

/**@file sot_refl.h
 * @brief Basic vector functions for reflection in the geometrical optics
 * framework.
 *
 * No assumptions of any physical properties are made on this level. The shape
 * of reflective surface is what only matters here. For the light attenuation,
 * polarization, phase shift, Goos-Hänchen spatial shift, etc, the below must be
 * used in conjunction with wave optics (i.e. Fresnel formulae).
 * This file declares few functions describing the optical ray reflection
 * in terms of geometrical options by utilizing the
 * <a href="https://en.wikipedia.org/wiki/Householder_transformation">Householder transformation</a>
 * with <a href="https://en.wikipedia.org/wiki/Specular_reflection">reflection matrix</a>.
 * For any implicitly declared surface \f$S(\vec{r}) = 0\f$, the reflected
 * (scattered) ray unit vector \f$\hat{d^\mathcal{s}}\f$ depends on incident
 * ray unit vector \f$\hat{d^\mathcal{i}}\f$ as follows: \f[
 * \hat{d^\mathcal{s}} = \mathbf{R} \cdot \hat{d^\mathcal{i}},
 * \f] where the Householder's matrix \f$\mathbf{R}\f$ may be expressed with
 * the gradient, naturally describing normal vector
 * \f$\hat{d^\mathcal{n}} = \nabla S(\vec{r})/|\nabla S(\vec{r})|\f$:
 * \f[
 * \mathbf{R} = \mathbf{I} - 2 \hat{d^\mathcal{n}} \hat{d^\mathcal{n}} = \mathbf{I} - \frac{2}{|\nabla S(\vec{r})|^2} \cdot \nabla S(\vec{r}) \cdot \nabla S(\vec{r})^T.
 * \label{eq:refLawGeneric}
 * \f]
 *
 * By notion
 * \f[
 * \mathcal{S}_{ij} = \frac{\partial S(\vec{r})}{\partial r_i} \frac{\partial S(\vec{r})}{\partial r_j} |\nabla S(\vec{r})|^{-2},
 * \f]
 * one can re-write the expression for the vector field of reflected rays:
 * \f[
 * \hat{d^{\mathcal{s}}_j} = \big[ \mathbf{I}_{ij} - 2 \cdot \mathcal{S}_{ij} \big] \hat{d^{\mathcal{i}}_{i}}.
 * \label{eq:refLaw}
 * \f]
 *
 * Besides the surface function \f$S(\vec{r})\f$ one has to provide the set of
 * its partial derivatives \f$\partial S(\vec{r})/\partial r_i\f$ for the
 * these numerical calculation routines.
 *
 * <h2>Examples</h2>
 * \image html side.svg  "Reflection on parabolic mirror: side view."
 * \image html 2.svg "Reflection on parabolic mirror: orthographic projection view."
 *
 * \warning The blunt sot_scattered2_at() function is inefficient due to number
 * of redundant trigonometric calls. It was introduced mostly for testing
 * purposes and may be wiped out in future releases.
 * */

# include "sot_surf.h"

/**@brief Householder's matrix calculus.
 *
 * Calculates Householder matrix \f$R\f$ at point \f$\vec{r}\f$ given by
 * gradient \f$g\f$. As a side effect, returns sum of squares of the gradient's
 * components. */
sot_Real_t
sot_householder_m( const sot_Vector3_t * g
                 , sot_SymMatrix_t * sm );

/**@brief Yields reflected ray direction with explicit gradient vector.
 *
 * At some point on surface with gradient \f$\vec{g}\f$, for given unit vector
 * \f$r_i\f$ of incident ray, calculates unit vector \f$r_s\f$ of scattered ray.
 * As a side effect, returns sum of squares of the gradient's components. */
sot_Real_t
sot_scattered( const sot_Vector3_t * g, const sot_Vector3_t * r_i
             , sot_Vector3_t * r_s );

/**@brief Yields reflected ray direction with implicit gradient vector calculus.
 *
 * At given point \f$\vec{r}\f$ on surface with gradient \f$\vec{g}\f$, for
 * given unit vector \f$r_i\f$ of incident ray, calculates unit vector
 * \f$r_s\f$ of scattered ray.
 * As a side effect, returns sum of squares of the gradient's components.
 *
 * \warning One may consider it being not very efficient when deals with
 * simultaneous refraction (transmission) as explicit gradient numbers are
 * needed there as well. */
sot_Real_t
sot_scattered_at( const sot_Vector3_t * r, const sot_SurfaceParameters_t * s, const sot_Vector3_t * r_i
           , sot_Vector3_t * r_o );

/**@brief Yields reflected ray direction with implicit gradient vector calculus
 * in terms of direction angles.
 *
 * At given point \f$\vec{r}\f$ on surface with gradient \f$\vec{g}\f$, for
 * given direction \f$d_i\f$ of incident ray, calculates direction \f$d_s\f$ of
 * scattered ray. Used for testing purposes. As a side effect, returns sum of
 * squares of the gradient's components.
 *
 * \warning This is a draft function that heavily uses expensive trigonometric
 * computations and it must not undergo heavy usage.
 * */
sot_Real_t
sot_scattered2_at( const sot_Vector3_t * r, const sot_SurfaceParameters_t * s, const sot_Direction_t * d_i
                 , sot_Direction_t * d_o );

# endif  /*H_SOT_REFL_H*/
