# ifndef H_SOT_REFRACTION_H
# define H_SOT_REFRACTION_H

/**@file sot_refr.h
 * @brief Basic vector functions for refraction in the geometrical optics
 * framework.
 *
 * Assuming light ray coming out from one medium to another, one may write out
 * simple relation between incident and reflected ray angles relative to normal
 * vector:
 * \f[
 * n_1 \sin \theta_1 = n_2 \sin \theta_2
 * \f]
 * known as Snell's law. Here, the \f$n_1\f$ and \f$n_2\f$ are refractive
 * indices of the media, \f$\theta_1\f$ and \f$theta_1\f$ are the angles b/w
 * the ray in media and normal vector.
 * */

# include "sot_types.h"

/**@brief Refraction vector as unit vectors form.
 *
 * For given normal unit vector n, given incident vector i and permeability
 * coefficient \f$\mu = n_1 / n_2\f$, calculates the reflection ray unit vector
 * \f$\vec{t}\f$ according to the following relation:
 *
 * \f[
 * \vec{t} = \sqrt{ 1 - \mu^2 \big[1 - (\vec{n} \cdot \vec{i})^2 \big] } \vec{n} + \mu \big[ \vec{i} - (\vec{n} \cdot \vec{i}) \vec{n} \big]
 * \f]
 *
 * Resulting unit vector may be further used in conjunction with the Fresnel
 * equations yielding the intensity ratios, or used per se in pure geometrical
 * applications neglecting light attenuation.
 *
 * For reference, see:
 *  https://physics.stackexchange.com/questions/435512/snells-law-in-vector-form
 * */
char
sot_refracted( const sot_Vector3_t * n
             , const sot_Vector3_t * i, sot_Real_t mu
             , sot_Vector3_t * t );

# endif  /* H_SOT_REFRACTION_H */
