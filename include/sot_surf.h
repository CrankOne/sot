# ifndef H_SOT_SURFACE_H
# define H_SOT_SURFACE_H

/**@brief Surface description routine.
 * @file sot_surf.h
 *
 * The surfaces supported by SOT is the core component for its CSG. It may be
 * described in few different ways:
 *
 * * implying the "precise" 2-nd order surface description using the
 *  combination of tensor-like objects,
 * * arbitrary function in implicit, 
 * * static conditioning using the C functions.
 *
 * Some of this functions have narrow practical meaning since were derived
 * for the sake of computational efficiency. Though, they're exposed in API
 * level for possible reentrant usage.
 * */

# include "sot_types.h"

# if defined(USE_GSL) && 1 == USE_GSL
#   include <gsl_math.h>
# endif

/**@brief Encodes parameters set for surface functions */
typedef enum sot_SurfaceType {
    /**@brief The generic implict function. */
    sot_kImplicit,
    /**@brief Surface described by the combination of function pointers */
    sot_kStatic,
    /**@brief Second order surface. */
    sot_kSecondOrder,
    /* ... */
} sot_SurfaceType_t;

/**@brief Parameters stack for surface functions.
 *
 * This structure represents a somewhat generalized parameters set:
 *  - sttc : surface gradient \f$\partial S / \partial r_i\f$
 *  - secOrd : Full parameters stack for 2-nd order surface given by quadratic
 *  form (\f$a_{ij}\f$), linear form (\f$l_i\f$) and free coefficient \f$d\f$.
 *  - any : parameters stack for arbitrary functor-like surface
 *
 * NOTE: this is inconclusive API!
 */
typedef struct sot_SurfaceParameters {
    /**@brief Particular surface type marker. */
    sot_SurfaceType_t type;
    union {
        /** Array of derivative functions for static surfaces (grad) */
        struct { sot_StaticScalarField_t dR[3]; } sttc;
        /** Parameters for arbitrary second order surface */
        struct {
            sot_SymMatrix_t a;
            sot_Vector3_t l;
            sot_Real_t d;
        } secOrd;
        /** Parameters stack for arbitrary functor-like parameterised surfaces;
         * the GSL-like derivative function has to be used to compute the
         * derivative. */
        struct {
            /* Surface as an implicit function to be differentiated, supporting
             * arbitrary parameters. */
            sot_Real_t (*f)( const sot_Vector3_t *, void * );
            /* Arbitrary implicit function parameters instance */
            void * parameters;
            /* Differentiation settings, GSL-like (typically,
             * gsl_deriv_central() may be provided */
            # if defined(USE_GSL) && 1 == USE_GSL
            int (*deriv_f)( const gsl_function *
                          , double x, double h, double * result, double * abserr );
            # else  /* defined(USE_GSL) && 1 == USE_GSL */
            int (*deriv_f)( double (*)( double, void * )
                          , double x, double h, double * result, double * abserr );
            # endif  /* defined(USE_GSL) && 1 == USE_GSL */
            /* Settings for the differentiating GSL-like function */
            double h;
        } implicit;
    };
} sot_SurfaceParameters_t;

/**@brief Gradient calculating function.
 *
 * Calculates \f$v = \vec{\nabla} S(\vec{r})\f$. Note, that error vector may be
 * NULL and will be re-written ONLY if numerical differentiation takes place
 * (i.e. in case of `sot_kImplicit' surfaces). */
void
sot_nabla_at( const sot_Vector3_t * r, const sot_SurfaceParameters_t * s
            , sot_Vector3_t * v, sot_Vector3_t * err );

/**@brief Open product calculating function.
 *
 * Calculates <a href="https://en.wikipedia.org/wiki/Tensor_product">oter product</a>
 * \f$\vec{v} \otimes \vec{v}^T\f$ and returns norm (as a side and useful
 * product of calculus) \f$|\vec{v}|^2\f$ */
sot_Real_t
sot_oprod( const sot_Vector3_t * g
         , sot_SymMatrix_t * sm);

/**@brief Vector contraction with quadratic form.
 *
 * Calculates double vector product with symmetric matrix (quadratic
 * form): \f$v_i \cdot a_{ij} \cdot v_j\f$ */
sot_Real_t
sot_quad_prod( const sot_SymMatrix_t * a, const sot_Vector3_t * v );

/**@brief Semi-symmetric vector contraction with symmetric matrix.
 *
 * Calculates asymmetric double vector product with symmetric matrix:
 * \f$u_i \cdot a_{ij} \cdot v_j\f$ */
sot_Real_t
sot_semi_quad_prod( const sot_Vector3_t * u
                  , const sot_SymMatrix_t * a
                  , const sot_Vector3_t * v );

/**@brief Parametric roots of 2-nd order surface and line intersection.
 *
 * Calculates intersection points parametric coordinates on the 2-nd order
 * surface given by \f$A_{ij}, l_{i}, d\f$ with line given by direction vector
 * \f$\vec{u_i}\f$ and point \f$\vec{r_0}\f$.
 *
 * @returns \c -1 in case no intersection found (no real roots in square equation)
 * @returns \c 1 in case of single intersection point
 * (\f$|t_1 - t_2| < \epsilon\f$). Only \c t[0] will be written.
 * @returns \c 2 in case of two instersection points, that will be written
 * in \c t[0], \c t[1].
 */
char
sot_quad_roots( const sot_SymMatrix_t * A, const sot_Vector3_t * l, const sot_Real_t d
              , const sot_Vector3_t * u, const sot_Vector3_t * r0
              , const sot_Real_t eps
              , sot_Real_t * t );

/**@brief Spatial points of 2-nd order surface and line intersection.
 *
 * For given 2-nd order surface and given ray, calculates intersection points,
 * based on parametric equation for the line.
 *
 * Effectively, calculates points from the roots of parametric equation yielded
 * by sot_quad_roots() function.
 * */
char
sot_ray_intersct_2nd( const sot_SymMatrix_t * A, const sot_Vector3_t * l, const sot_Real_t d
                    , const sot_Vector3_t * u, const sot_Vector3_t * r0
                    , const sot_Real_t eps
                    , sot_Real_t * t
                    , sot_Vector3_t * x );

# endif  /* H_SOT_SURFACE_H */
