# ifndef H_SOT_TYPES_H
# define H_SOT_TYPES_H

/**@file sot_types.h
 * @brief Defines basic common types for SOT library.
 *
 * Types of real number, spatial vector, symmetric matrix, indexing and few
 * simple geometrical operations complementary to that types are defined in
 * this file.
 *
 * The rationale for implementing our own vector algebra here is that we want
 * simplistic approach of describing few basic vector relations in pure C,
 * offering ability to express plain 3-component array as a vector with named
 * and enumerated components.
 * */

# include <stdlib.h>
# include <math.h>
# include <stdio.h>
# include <assert.h>

/**@brief Defines the real number type for SOT library */
# ifndef REAL_NUMBER_TYPE
#   define REAL_NUMBER_TYPE float
# endif

/**@brief Real number type for SOT library */
typedef REAL_NUMBER_TYPE sot_Real_t;

/**@brief (short) index numeric type for SOT library, indexing matirces, vertexes,
 * etc. */
typedef unsigned char sot_Index_t;

/**@brief 3-dimensional vector */
typedef union sot_Vector3 {
    /** Named components */
    struct { sot_Real_t x, y, z; };
    /** Enumerated components */
    sot_Real_t r[3];
} sot_Vector3_t;

/**@brief 2-angle direction in spherical space.
 * For the sake of performance, avoid using this function frequently. The
 * trig functions cost much. */
typedef union sot_Direction {
    /** Named angular components */
    struct { sot_Real_t theta, phi; };
    /** Enumerated angular componenets */
    sot_Real_t angles[2];
} sot_Direction_t;

/**@brief 9-component matrix which is effectively 6-component (symmetrical) */
typedef struct sot_SymMatrix {
    /** Elementsi index. See sot_gSymMatrixIndex for order reference. */
    sot_Real_t el[6];
} sot_SymMatrix_t;

/**@brief Mapping of linear indices within sot_SymMatrix struct into 3x3
 * matrix. */
extern const sot_Index_t sot_gSymMatrixIndex[3][3];

/**@brief Type of function pointer defining static scalar field */
typedef sot_Real_t (*sot_StaticScalarField_t)( const sot_Vector3_t * r );

/**@brief Type of function pointer defining static vector field */
typedef void (*sot_StaticVectorField_t)( const sot_Vector3_t * r, sot_Vector3_t * v );

/**@brief Calculates scalar product of vectors */
sot_Real_t
sot_scalar_prod( const sot_Vector3_t * a
               , const sot_Vector3_t * b );

/**@brief In-place product of vector and scalar */
void
sot_sprod_self( sot_Real_t s, sot_Vector3_t * v );

/**@brief Writes direction of vector. */
void
sot_direction_of( const sot_Vector3_t * v
                , sot_Direction_t * d );

/**@brief For given 3-vector \f$\vec{v}\f$, derives the unit 3-vector,
 * returning norm. */
sot_Real_t
sot_ort_of( const sot_Vector3_t * v
          , sot_Vector3_t * ortV );

/**@brief In-place ort vector, returns norm value */
sot_Real_t
normalize( sot_Vector3_t * ortV );

# endif  /*H_SOT_TYPES_H*/

