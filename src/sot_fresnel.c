# include "sot_fresnel.h"

void
sot_fresnel_ratios_generic( sot_Real_t n1, sot_Real_t mu1
                          , sot_Real_t n2, sot_Real_t mu2
                          , sot_Real_t cosThetaT, sot_Real_t cosThetaI
                          , sot_Real_t * r_P, sot_Real_t * t_P
                          , sot_Real_t * r_S, sot_Real_t * t_S ) {
    const sot_Real_t n1m1 = n1/mu1
                   , n2m2 = n2/mu2
                   /* (7) */
                   , r_P1 = n1m1*cosThetaT
                   , r_P2 = n2m2*cosThetaI
                   , r_P3 = r_P1 + r_P2
                   /* (8) */
                   , r_S1 = n1m1*cosThetaI
                   /* (12) */
                   , r_S2 = n2m2*cosThetaT
                   , r_S3 = r_S1 + r_S2
                   ;
    *r_P = (r_P1 - r_P2)/r_P3;
    *t_P = 2* r_S1 / r_P3;
    *t_S = 2* r_S1 / r_S3;
    *r_S = (r_S1 - r_S2)/r_S3;
}

void
sot_fresnel_ratios( sot_Real_t thetaI, sot_Real_t thetaT
                  , sot_Real_t * r_P, sot_Real_t * t_P
                  , sot_Real_t * r_S, sot_Real_t * t_S ) {
    const sot_Real_t sinTipTt = sin( thetaI + thetaT )
                   , sinTT = sin( thetaT )
                   , cosTI = cos( thetaI )
                   ;
    *r_P = - tan( thetaI - thetaT ) / tan( thetaI + thetaT );
    *t_P = 2 * sinTT * cosTI / ( sinTipTt * cos( thetaI - thetaT ) );
    *r_S = - sin( thetaI - thetaT ) / sinTipTt;
    *t_S = 2*sinTT*cosTI/sinTipTt;
}

