# include "sot_octree.h"
# include "sot_error.h"

# include <string.h>

sot_Octree_t *
sot_octree_pool_alloc( sot_OctreePool_t * pool ) {
    if( pool->occupied >= pool->poolSize ) {
        SOT_THROW( PoolOverflow, "Unable to allocate node on pool %p;"
                " pool of size %zu exceed.", pool, pool->poolSize );
    }
    bzero( pool->elements + pool->occupied, sizeof(*pool->elements) );
    pool->occupied += 1;
    return pool->elements;
}

size_t
sot_make_octree( sot_OctreePool_t * pool
               , sot_Body_t * body
               , const sot_Vector3_t * O, sot_Real_t side, sot_Real_t smallestSide ) {
    size_t count = 0;
    /* Enumerates the 8 octants (sequetially) */
    sot_OctreeBits8 n = 0;
    /* Denotes the end of the octant */
    sot_Vector3_t probeEnd = {{ O->x + side
                              , O->y + side
                              , O->z + side }}
               , subO;
    const sot_Real_t s2 = side/2;
    /* Node currently considered */
    sot_Octree_t * node = sot_octree_pool_alloc( pool );
    count += 1;
    /* Get octants filled with the body */
    node->fillBits = body->intersects_f( body->p, O, &probeEnd );
    /* Iterate over the octants */
    for( sot_OctreeBits8 octantN = 1
       ; octantN ; octantN <<= 1, ++n ) {
        /* If no body part encompassed, keep going */
        if( ! (octantN & node->fillBits) ) continue;
        /* Otherwise, go on recursively within this sub-octant: */
        memcpy( subO.r, O->r, sizeof( O->r ) );
        if( 0x1 & n ) subO.x += s2;
        if( 0x2 & n ) subO.y += s2;
        if( 0x3 & n ) subO.z += s2;
        count += sot_make_octree( pool, body, &subO, s2, smallestSide );
    }
    return count;
}

