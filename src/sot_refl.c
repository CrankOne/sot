# include "sot_refl.h"

sot_Real_t
sot_householder_m( const sot_Vector3_t * g
                 , sot_SymMatrix_t * sm ) {
    sot_Real_t norm = sot_oprod( g, sm );
    sm->el[0] = 1. - 2*sm->el[0]/norm;
    sm->el[1] = 1. - 2*sm->el[1]/norm;
    sm->el[2] = 1. - 2*sm->el[2]/norm;
    sm->el[3] =    - 2*sm->el[3]/norm;
    sm->el[4] =    - 2*sm->el[4]/norm;
    sm->el[5] =    - 2*sm->el[5]/norm;
    return norm;
}

sot_Real_t
sot_scattered( const sot_Vector3_t * g, const sot_Vector3_t * r_i
             , sot_Vector3_t * r_o ) {
    sot_SymMatrix_t hm;
    const sot_Index_t Rix[9] = { 0, 3, 5
                               , 3, 1, 4
                               , 5, 4, 2 }
                , * rixPtr = Rix;
    sot_Real_t g2 = sot_householder_m( g, &hm ); 
    for( sot_Index_t i = 0; i < 3; ++i ) {
        r_o->r[i] = 0;
        for( sot_Index_t j = 0; j < 3; ++j ) {
            r_o->r[i] += r_i->r[j] * hm.el[*rixPtr++];
        }
    }
    return g2;
}

sot_Real_t
sot_scattered_at( const sot_Vector3_t * r, const sot_SurfaceParameters_t * s, const sot_Vector3_t * r_i
                , sot_Vector3_t * r_o ) {
    sot_Vector3_t g;
    sot_nabla_at( r, s, &g, NULL );   /* MOVE */
    return sot_scattered( &g, r_i, r_o );
}

sot_Real_t
sot_scattered2_at( const sot_Vector3_t * r, const sot_SurfaceParameters_t * s, const sot_Direction_t * d_i
                , sot_Direction_t * d_o ) {
    /* TODO: can we optimize this? */
    sot_Vector3_t r_i = {{ sin(d_i->theta)*cos(d_i->phi)
                    , sin(d_i->theta)*sin(d_i->phi)
                    , cos(d_i->theta)
                    }}
         , r_o;
    sot_Real_t g2 = sot_scattered_at( r, s, &r_i, &r_o );
    sot_direction_of( &r_o, d_o );
    return g2;
}

