# include <string.h>
# include "sot_refr.h"

char
sot_refracted( const sot_Vector3_t * n
             , const sot_Vector3_t * i, sot_Real_t mu
             , sot_Vector3_t * t ) {
    sot_Index_t k;
    const sot_Real_t ni = sot_scalar_prod(n, i)
                   , sqc = 1 - mu*mu*(1 - ni*ni)
                   ;
    assert(sqc >= 0);
    memcpy( t->r, n->r, sizeof(n->r) );
    sot_sprod_self( sqrt(sqc), t );
    for( k = 0; k < 3; ++k ) {
        t->r[k] += mu*(i->r[k] - n->r[k]*ni);
    }
    return 0;
}

