# include "sot_surf.h"
# include "sot_error.h"

struct _sot_GSL_FWrapper {
    /* Examined function */
    sot_Real_t (*f)(const sot_Vector3_t *, void *);
    /* Own parameters of examined function */
    void * ownParameters;
    /* Values of other variables */
    sot_Vector3_t r;
    /* Examined dimension */
    sot_Index_t i;
};

static double _gsl_wrapper(double v, void * p_) {
    assert(p_);
    struct _sot_GSL_FWrapper * p = (struct _sot_GSL_FWrapper *) p_;
    p->r.r[p->i] = v;
    return p->f( &(p->r), p->ownParameters );
}

void
sot_nabla_at( const sot_Vector3_t * r, const sot_SurfaceParameters_t * s
            , sot_Vector3_t * v, sot_Vector3_t * err ) {
    assert( r );
    assert( s );
    assert( v );
    if( sot_kStatic == s->type ) {
        for( sot_Index_t i = 0; i < 3; ++i ) {
            v->r[i] = s->sttc.dR[i](r);
        }
    } else if( sot_kSecondOrder == s->type ) {
        for( sot_Index_t i = 0; i < 3; ++i ) {
            v->r[i] = 0;
            for( sot_Index_t j = 0; j < 3; ++j ) {
                v->r[i] += s->secOrd.a.el[sot_gSymMatrixIndex[i][j]] * r->r[j];
            }
            v->r[i] += s->secOrd.l.r[i];
            v->r[i] *= 2;
        }
    } else if( sot_kImplicit == s->type ) {
        struct _sot_GSL_FWrapper sgfw = {
                s->implicit.f,
                s->implicit.parameters
                /* r, i is set below, in loop */
            };
        # if defined(USE_GSL) && 1 == USE_GSL
        gsl_function gslw = { _gsl_wrapper, &sgfw };
        # endif
        double valCm, errCm;
        for(sot_Index_t i = 0; i < 3; ++i) {
            /* Components of the vector r provided by `sgfw' parameters stack
             * may be re-written in previous iteration, ba call made within
             * the numerical differentiation algorithm (within the
             * _gsl_wrapper() call). Despite, this re-writing might be
             * redundant, it is kept for a while. Aware this possible side
             * effect prior to any optimization. */
            memcpy( sgfw.r.r, r->r, sizeof(sot_Vector3_t) );
            sgfw.i = i;
            # if defined(USE_GSL) && 1 == USE_GSL
            s->implicit.deriv_f( &gslw, r->r[i], s->implicit.h
                               , &valCm, &errCm );
            # else
            s->implicit.deriv_f( _gsl_wrapper, r->r[i], s->implicit.h
                               , &valCm, &errCm );
            # endif
            v->r[i] = valCm;
            if(err) {
                err->r[i] = errCm;
            }
        }
    } else {
        SOT_THROW( NotImplemented
                  , "Function derivatives for %0x is implemented."
                  , (int) s->type )
    }
}

sot_Real_t
sot_oprod( const sot_Vector3_t * g
         , sot_SymMatrix_t * sm ) {
    assert(g);
    assert(sm);
    sm->el[0] = g->x*g->x; /*1, 1*/
    sm->el[1] = g->y*g->y; /*2, 2*/
    sm->el[2] = g->z*g->z; /*3, 3*/
    sm->el[3] = g->x*g->y; /*1, 2*/
    sm->el[4] = g->y*g->z; /*2, 3*/
    sm->el[5] = g->x*g->z; /*1, 3*/
    return sm->el[0] + sm->el[1] + sm->el[2];
}

sot_Real_t
sot_quad_prod( const sot_SymMatrix_t * a, const sot_Vector3_t * v ) {
    assert(a);
    assert(v);
    return a->el[0] * v->x * v->x
         + a->el[1] * v->y * v->y
         + a->el[2] * v->z * v->z
         + 2*( a->el[3] * v->x * v->y
             + a->el[4] * v->y * v->z
             + a->el[5] * v->x * v->z )
         ;
}

sot_Real_t
sot_semi_quad_prod( const sot_Vector3_t * u
                  , const sot_SymMatrix_t * a
                  , const sot_Vector3_t * v ) {
    assert(u);
    assert(a);
    assert(v);
    return (a->el[5] * u->r[2] + a->el[3] * u->r[1] + a->el[0]*u->r[0])*v->r[0]
         + (a->el[4] * u->r[2] + a->el[1] * u->r[1] + a->el[3]*u->r[0])*v->r[1]
         + (a->el[2] * u->r[2] + a->el[4] * u->r[1] + a->el[5]*u->r[0])*v->r[2]
         ;
}

char
sot_quad_roots( const sot_SymMatrix_t * A, const sot_Vector3_t * l, const sot_Real_t d
              , const sot_Vector3_t * u, const sot_Vector3_t * r0
              , const sot_Real_t eps
              , sot_Real_t * t ) {
    const sot_Real_t a = sot_quad_prod( A, u )
                   , b = 2*sot_semi_quad_prod( r0, A, u ) + 2*sot_scalar_prod( l, u )
                   , c = sot_quad_prod( A, r0 ) + 2*sot_scalar_prod( l, r0 ) + d
                   , D = b*b - 4*a*c
                   ;
    if( D < 0. ) {
        return -1;
    }
    if( !a ) {
        return -2;  /* TODO:? */
    }
    if( D < eps ) {
        t[0] = - b/(2*a);
        return 1;
    } else {
        const sot_Real_t sqD = sqrt(D)
                       , a2 = 2*a
                       ;
        t[0] = - (b + sqD)/a2;
        t[1] = - (b - sqD)/a2;
        return 2;
    }
}

/** For given 2-nd order surface and given ray, calculates intersection points,
 * based on parametric equation for the line.
 */
char
sot_ray_intersct_2nd( const sot_SymMatrix_t * A, const sot_Vector3_t * l, const sot_Real_t d
                    , const sot_Vector3_t * u, const sot_Vector3_t * r0
                    , const sot_Real_t eps
                    , sot_Real_t * t
                    , sot_Vector3_t * x ) {
    sot_Index_t i, j;
    char nr = sot_quad_roots( A, l, d, u, r0, eps, t );
    if( nr < 0) return nr;
    if( 1 == nr ) {
        for( i = 0; i < 3; ++i ) x[0].r[i] = r0->r[i] + t[0]*u->r[i];
        return nr;
    } else {
        for( j = 0; j < 2; ++j )
            for( i = 0; i < 3; ++i )
                x[j].r[i] = r0->r[i] + t[j]*u->r[i];
        return nr;
    }
}

