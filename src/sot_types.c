# include "sot_types.h"

const sot_Index_t sot_gSymMatrixIndex[3][3] = {
    { 0, 3, 5 },
    { 3, 1, 4 },
    { 5, 4, 2 }
};

sot_Real_t
sot_scalar_prod( const sot_Vector3_t * a, const sot_Vector3_t * b ) {
    assert(a);
    assert(b);
    return a->x * b->x
         + a->y * b->y
         + a->z * b->z
         ;
}

void
sot_sprod_self( sot_Real_t s, sot_Vector3_t * v ) {
    v->x *= s;
    v->y *= s;
    v->z *= s;
}

void sot_direction_of( const sot_Vector3_t * v
                     , sot_Direction_t * d ) {
    d->theta = atan2( sqrt(v->x * v->x + v->y * v->y), v->z );
    d->phi = atan2(v->y, v->x);
}

sot_Real_t
sot_ort_of( const sot_Vector3_t * v
          , sot_Vector3_t * ortV ) {
    const sot_Real_t norm = sqrt(v->x*v->x + v->y*v->y + v->z*v->z);
    ortV->x = v->x / norm;
    ortV->y = v->y / norm;
    ortV->z = v->z / norm;
    return norm;
}

sot_Real_t
normalize( sot_Vector3_t * v ) {
    const sot_Real_t norm = sqrt(v->x*v->x + v->y*v->y + v->z*v->z);
    v->x /= norm;
    v->y /= norm;
    v->z /= norm;
    return norm;
}

